![App Screen](/Screenshot_20210610-153042_Ioasys_Empresas.png)

# PROJEO EMPRESAS #

* Aplicativo Android utilizando linguagem Kotlin


### BIBLIOTECAS ###

As bibliotecas que precisaram ser inseridas foram:

* Retrofit2 - REST API
* Glide - para manusear  stream de imagens 
* GSON - para converter Json em Objeto
* Coroutines -  programação assíncrona no Android

Como justificativa geral a escolha das bibliotecas é feita considerando os cases de uso, documentação 
e envolvimento da comunidade de programadores com o uso das bibliotecas. Assim fica fácil obter suporte para suas utilizações

### TO-DO ###

Neste projeto faltou tempo de implementar os testes e organizar e organiza-lo numa arquitetura como MVVM que está 
bem difundida na comunidade. Algumas funcionalidades não ficaram 100% fiel a proposta, mas por pura falta de tempo mesmo.

### COMO EXECUTAR O PROJETO ###

A forma mais simples de executar o projeto é utilizando a ide Android Studio V-4.2.1.
Android Studio -> Janela principal -> Get From Version Control 
-> URL: https://hillanvieira@bitbucket.org/hillanvieira/empresas-android.git -> Clone.

Ou abra um terminal digite o comando: 
* git clone https://hillanvieira@bitbucket.org/hillanvieira/empresas-android.git
na pasta que deseja clonar o projeto e no Android Studio -> Open an Existing Project -> Selecione o caminho :).

Para facilitar a execução do app adicionei um .apk no repositório no caminho: empresas-android/app/build/outputs/apk/debug/app-debug.apk
só instalar o apk num emulador ou direto num smarthphone.

### CONCLUSÃO ###

O projeto foi finalizado com 90% do que foi proposto na tarefa, por eu não ter mais tempo não implementei os bonus mas
esse outro projeto que eu fiz recentemente >>https://github.com/hillanvieira/GitIssues<< utiliza: 
Room, LiveData, Coroutines, ViewModel e Koin.

Foi um prazer realizar esse desafio e espero o feedback de vocês.