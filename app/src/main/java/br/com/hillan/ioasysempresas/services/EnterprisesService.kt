package br.com.hillan.ioasysempresas.services

import br.com.hillan.ioasysempresas.model.Auth
import br.com.hillan.ioasysempresas.model.Enterprises
import br.com.hillan.ioasysempresas.model.LoginInfo
import retrofit2.Call
import retrofit2.http.*

interface EnterprisesService {
    @Headers("Content-Type: application/json; charset=urf-8")
    @POST("/api/v1/users/auth/sign_in")
    fun getAuth(@Body loginInfo: LoginInfo): Call<Auth>


//    @Headers(
//        "access-token: mXXDB4GxO8ujLRmgRqcQUQ",
//        "client: -iQqMR9GKAEteKf_CWftUA",
//        "uid: testeapple@ioasys.com.br"
//    )
    @GET("/api/v1/enterprises")
    fun getEnterprises(
        @Header("access-token") accessToken: String?,
        @Header("client") client: String?,
        @Header("uid") uid: String?,
    ): Call<Enterprises>
}