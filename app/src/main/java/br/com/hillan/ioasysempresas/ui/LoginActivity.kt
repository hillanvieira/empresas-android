package br.com.hillan.ioasysempresas.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.view.View.OnTouchListener
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import br.com.hillan.ioasysempresas.R
import br.com.hillan.ioasysempresas.model.HeadersAccess
import br.com.hillan.ioasysempresas.model.LoginInfo
import br.com.hillan.ioasysempresas.services.RestApiService

class LoginActivity : AppCompatActivity() {

    private var emailField: EditText? = null
    private var passwordField: EditText? = null
    private var progress: ProgressBar? = null
    private var progressBackground: LinearLayout? = null
    private var button: Button? = null
    private var errorMessage: TextView? = null
    private var errorShow: Boolean = false


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val apiService = RestApiService()

        //Hide actionbar
        if (supportActionBar != null) {
            supportActionBar?.hide()
        }

        emailField = findViewById(R.id.editTextTextEmailAddress)
        passwordField = findViewById(R.id.editTextTextPassword)
        button = findViewById(R.id.LoginButton)
        progress = findViewById(R.id.progressBar)
        progressBackground = findViewById(R.id.progressBackground)
        errorMessage = findViewById(R.id.errorMessage)

        configureView()

        emailField?.setOnTouchListener(OnTouchListener{view,event ->
            if(errorShow){
                errorShow = false
                configureView()
            }
            false
        })

        passwordField?.setOnTouchListener(OnTouchListener { _, event ->

            if(errorShow){
                errorShow = false
                configureView()
            }

            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3
            if (event.action == MotionEvent.ACTION_UP) {

                if (event.rawX >= passwordField?.right!! - passwordField?.compoundDrawables?.get(DRAWABLE_RIGHT)?.bounds?.width()!!) {

                    if (passwordField?.transformationMethod == HideReturnsTransformationMethod.getInstance()) {

                        passwordField?.transformationMethod =
                            PasswordTransformationMethod.getInstance()
                    } else {
                        passwordField?.transformationMethod =
                            HideReturnsTransformationMethod.getInstance()
                    }

                    return@OnTouchListener true
                }
            }
            false
        })

        button?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                var email: String = emailField?.text.toString()
                var password: String = passwordField?.text.toString()

                showProgress()

                apiService.loginAccess(LoginInfo(email, password)) {

                 hideProgress()

                    if (it != null) {
                        Log.d("Response", "Success")
//                        Toast.makeText(v?.context, "Success", Toast.LENGTH_LONG).show()
                        goToMainActivity(it)

                    } else {
//                        Toast.makeText(
//                            v?.context,
//                            "Invalid login credentials. Please try again.",
//                            Toast.LENGTH_LONG
//                        ).show()
                        configureErroLogin()

                    }
                }

            }
        })
    }




    private fun goToMainActivity(headersAccess: HeadersAccess){
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("ACCESS_TOKEN", headersAccess.accessToken)
        intent.putExtra("CLIENT", headersAccess.client)
        intent.putExtra("UID", headersAccess.uid)
        startActivity(intent)
    }

    private fun showProgress(){
        progressBackground?.visibility = VISIBLE
        progress?.visibility = VISIBLE
    }

    private fun hideProgress(){
        progressBackground?.visibility = INVISIBLE
        progress?.visibility = INVISIBLE
    }

    private fun configureErroLogin() {
        emailField?.setCompoundDrawablesWithIntrinsicBounds(
            resources.getDrawable(R.drawable.ic_letter),
            null,
            resources.getDrawable(R.drawable.ic_info),
            null
        )
        passwordField?.setCompoundDrawablesWithIntrinsicBounds(
            resources.getDrawable(R.drawable.ic_lock),
            null,
            resources.getDrawable(R.drawable.ic_info),
            null
        )
        errorMessage?.visibility = VISIBLE

        button?.setBackgroundColor(
            ResourcesCompat.getColor(
                resources,
                R.color.steel_grey,
                null
            )
        )

        passwordField?.transformationMethod = PasswordTransformationMethod.getInstance()

        errorShow = true

    }

    private fun configureView() {
        emailField?.setCompoundDrawablesWithIntrinsicBounds(
            resources.getDrawable(R.drawable.ic_letter),
            null,
            null,
            null
        )
        passwordField?.setCompoundDrawablesWithIntrinsicBounds(
            resources.getDrawable(R.drawable.ic_lock),
            null,
            resources.getDrawable(R.drawable.ic_eye),
            null
        )
        errorMessage?.visibility = INVISIBLE
        button?.setBackgroundColor(
            ResourcesCompat.getColor(
                resources,
                R.color.blue_pool,
                null
            )
        )

    }


}