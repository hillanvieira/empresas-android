package br.com.hillan.ioasysempresas.services

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://empresas.ioasys.com.br"

object RetrofitInitializer {
    private val client = OkHttpClient.Builder().build()

    val retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

    fun buildRestService(): EnterprisesService {
        return retrofit.create(EnterprisesService::class.java)
    }

}