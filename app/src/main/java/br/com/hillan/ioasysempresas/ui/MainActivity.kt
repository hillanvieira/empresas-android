package br.com.hillan.ioasysempresas.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.View.*
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.hillan.ioasysempresas.R
import br.com.hillan.ioasysempresas.adaper.EnterpriseListAdapter
import br.com.hillan.ioasysempresas.model.Enterprise
import br.com.hillan.ioasysempresas.model.Enterprises
import br.com.hillan.ioasysempresas.model.HeadersAccess
import br.com.hillan.ioasysempresas.services.RestApiService

class MainActivity : AppCompatActivity() {

    private var authForGet: HeadersAccess? = null
    private var mainText: TextView? = null
    private lateinit var enterprises: Enterprises


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //setting toolbar
        val toolbar = findViewById<View>(R.id.search_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))

        title = ""

        mainText = findViewById(R.id.main_text)

        val apiService = RestApiService()

        authForGet = getAuthData()

        apiService.getEnterprises(authForGet!!) { it ->
            Log.d("enterprise", it.toString())
            if (it != null) {
                enterprises = it
//                for (item in it.enterprises) {
//                    Log.d("enterprise", item.ententerpriseType.enterpriseTypeName)
//                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        menuInflater.inflate(R.menu.main_menu, menu)

        val searchItem = menu.findItem(R.id.app_bar_search)

        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView

            val searchHint = getString(R.string.searchHint)
            searchView.queryHint = searchHint

            searchView.setOnSearchClickListener {
                mainText?.visibility = INVISIBLE
            }

            searchView.setOnCloseListener {
                mainText?.visibility = VISIBLE
              false
            }

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText!!.toString().isNotEmpty()) {

                        val listEnterprise: MutableList<Enterprise> = mutableListOf()

                        for (search in enterprises.enterprises) {

                            if(search.ententerpriseName.contains(newText,true) && newText != ""){
                                listEnterprise.add(search)
                            }
//                            if(newText.equals(search.ententerpriseName)){
//                                listEnterprise.add(search)
//                            }
                        }
                        configurerecyclerView(listEnterprise)

                        // startRecyclerView(generateData(newText))
                        // companyList.clear()

                    } else {
                        configurerecyclerView(emptyList())
                        // startRecyclerView(generateData(newText))
                        // companyList.clear()
                    }
                    return false
                }
            })
        }
        return super.onCreateOptionsMenu(menu)
    }

    fun configurerecyclerView(enterprises: List<Enterprise>) {
        val recyclerView: RecyclerView = findViewById(R.id.search_recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = EnterpriseListAdapter(enterprises, this)
        {
//            Toast.makeText(
//                this,
//                "${it.ententerpriseName} Clicked",
//                Toast.LENGTH_LONG
//            ).show()

            goToDetailActivity(it)
        }
    }

    private fun goToDetailActivity(enterprise: Enterprise){
        val intent = Intent(this, ViewDetailsActivity::class.java)
        intent.putExtra("DETAILS", enterprise.description)
        intent.putExtra("PHOTO_URL", enterprise.photo)
        intent.putExtra("NAME", enterprise.ententerpriseName)
        startActivity(intent)
    }

    private fun getAuthData(): HeadersAccess {
        val intent = intent
        val headersAccess: HeadersAccess = HeadersAccess(
            intent.getStringExtra("ACCESS_TOKEN")!!,
            intent.getStringExtra("CLIENT")!!,
            intent.getStringExtra("UID")!!
        )
        return headersAccess
    }
}