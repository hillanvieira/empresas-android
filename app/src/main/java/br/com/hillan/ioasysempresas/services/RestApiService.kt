package br.com.hillan.ioasysempresas.services

import br.com.hillan.ioasysempresas.model.Auth
import br.com.hillan.ioasysempresas.model.Enterprises
import br.com.hillan.ioasysempresas.model.HeadersAccess
import br.com.hillan.ioasysempresas.model.LoginInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestApiService {

    fun loginAccess(loginInfo: LoginInfo, onResult: (HeadersAccess?) -> Unit) {
        val retrofit = RetrofitInitializer.buildRestService()
        retrofit.getAuth(loginInfo).enqueue(
            object : Callback<Auth> {
                override fun onFailure(call: Call<Auth>, t: Throwable) {
                    onResult(null)
                }

                override fun onResponse(call: Call<Auth>, response: Response<Auth>) {

                    if (response.body()?.success == true) {

                        onResult(
                            HeadersAccess(
                                response.headers().get("access-token")!!,
                                response.headers().get("client")!!,
                                response.headers().get("uid")!!
                            )
                        )
                    } else {
                        onResult(null)
                    }

                    //val jObjError = JSONObject(response.errorBody()!!.string())
                    //val loginResponse = response.body()
                    //onResult(loginResponse)
                }
            }
        )
    }

    fun getEnterprises(headersAccess: HeadersAccess, onResult: (Enterprises?) -> Unit) {
        val retrofit = RetrofitInitializer.buildRestService()
        retrofit.getEnterprises(
            headersAccess.accessToken,
            headersAccess.client,
            headersAccess.uid
        ).enqueue(
                object : Callback<Enterprises> {
                    override fun onResponse(
                        call: Call<Enterprises>,
                        response: Response<Enterprises>
                    ) {

                        if (response.body()!= null){
                            onResult(response.body())
                        }else{
                            onResult(null)
                        }

                    }

                    override fun onFailure(call: Call<Enterprises>, t: Throwable) {
                        onResult(null)
                    }

                }
            )

    }

}