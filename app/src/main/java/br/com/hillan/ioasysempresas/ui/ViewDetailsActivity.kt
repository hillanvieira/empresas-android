package br.com.hillan.ioasysempresas.ui

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import br.com.hillan.ioasysempresas.R
import br.com.hillan.ioasysempresas.model.Enterprise
import br.com.hillan.ioasysempresas.model.Type
import br.com.hillan.ioasysempresas.services.BASE_URL
import com.bumptech.glide.Glide

class ViewDetailsActivity : AppCompatActivity() {

    private var imageView: ImageView? = null
    private var description: TextView? = null
    private var enterprise:  Enterprise? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_details)

        val toolbar = findViewById<View>(R.id.detail_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))

        enterprise = getDetailsData()

        imageView = findViewById(R.id.imageViewDetail)
        description = findViewById(R.id.textViewDetail)

        title = enterprise?.ententerpriseName
        description?.text =  enterprise?.description

        Glide.with(this).load("$BASE_URL${enterprise?.photo}")
            .placeholder(R.drawable.logo_home)
            .error(R.drawable.logo_home)
            .centerCrop()
            .into(imageView!!)
    }


    private fun getDetailsData(): Enterprise {
        val intent = intent
        val enterprise: Enterprise = Enterprise(
            intent.getStringExtra("PHOTO_URL")!!,
            intent.getStringExtra("NAME")!!,
            intent.getStringExtra("DETAILS")!!,
            "",
            Type("")
        )

        return enterprise
    }
}