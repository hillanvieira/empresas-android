package br.com.hillan.ioasysempresas.model

import com.google.gson.annotations.SerializedName

class Enterprises(
    @SerializedName("enterprises") val enterprises: ArrayList<Enterprise>
)

class Enterprise(
     val photo: String,
     @SerializedName("enterprise_name") val ententerpriseName:String,
     val description: String,
     val country: String,
     @SerializedName("enterprise_type") val ententerpriseType:Type
)

class Type(
    @SerializedName("enterprise_type_name") val enterpriseTypeName: String
)